var gulp   	 = require('gulp'), 
    sass   	 = require('gulp-sass'),
    uglify 	 = require('gulp-uglify'),
    rename 	 = require('gulp-rename'),
    version  = require('gulp-version-number'),
    cleancss = require('gulp-clean-css');

// for css and js versions 
const currentVersion = '0.1.0';

gulp.task('styles', function(){ 
    return gulp.src('styles/**/*.{sass,scss}') 
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('styles')) 
});

gulp.task('scripts', function (){
	return gulp.src('scripts/*.js')
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('scripts/min-scripts/'))
});

gulp.task('watch', function(){
    gulp.watch('styles/**/*.{sass,scss}', gulp.parallel('styles'));
    gulp.watch('scripts/*.js', gulp.parallel('scripts'));
});

gulp.task('version', function(){
	return gulp.src('index.html', {base: './'})
		.pipe(version({
			'value' : currentVersion,
  			'append': {
    			'key': '_v',
    			'cover' : 0,
    			'to': ['css', 'js']
			},

			/*'output' : {
				'file' : 'version.json'
			}*/	
		}))
		.pipe(gulp.dest('./'));
});

gulp.task('default', gulp.parallel('styles', 'scripts', 'version', 'watch'))﻿;

