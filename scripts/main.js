$(document).ready(function(){
	$(".hamburger-menu").click(function(){
		event.preventDefault();
		$("#sidebar").toggleClass("visible");
		$(".hamburger-item").toggleClass("active");
	});

	$(".sidebar-item").click(function(){
		$("#sidebar").removeClass("visible");
		$(".hamburger-item").removeClass('active');
	});

	$(".sidebar-link, .sidebar-brand").click(function(){
		event.preventDefault();

		$("html, body").animate({
			scrollTop: $($(this).attr('href')).offset().top
		}, 500, "linear");
	}); 
});
